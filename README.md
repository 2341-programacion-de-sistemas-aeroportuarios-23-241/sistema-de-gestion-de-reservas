# Programación de Sistemas Aeroportuarios
# Práctica 4. Sistema de Gestión de Reservas - Preliminar

**Objetivos:** En esta práctica tendrás que desarrollar en Python un sistema de gestión de reservas de vuelos. Esta aplicación debe ser capaz de mostrar la información de un vuelo, sus pasajeros, asientos ocupados/libres y gestionar su información.

**Conocimientos teóricos previos:** Conocimientos básicos de Python.

**Tiempo de la práctica:** El tiempo estimado para la realización de esta práctica es de 4 horas.

**Fecha de entrega de la pŕactica:** Esta práctica SÍ requiere ser entregada

**Fecha límite de entrega:** El día 12/11/2023 a las 24h.

## Documentación de la práctica

La práctica deberá tener un documento que resuma el funcionamiento del código. Además, deberá explicar los siguientes conceptos teóricos y su utilización en la práctica:
-  Concepto de estructuras de datos y su uso en la práctica
-  Concepto de prueba unitaria y su uso en la práctica


## Pasos previos a la práctica
Debe crear un proyecto privado en su cuenta de gitlab con el nombre de "psa23-p1-gestionreservas". Es importante que mantenga ese nombre ya que se utilizará para la corrección de la práctica. Debe ser un nuevo proyecto. Aunque reutilice los ficheros de este proyecto, el alumno debe copiar los ficheros a este nuevo proyecto.

El proyecto debe ser privado y no visible a ningún compañero. Se pasarán comprobaciones para detectar la copia de código.

Dentro de ese proyecto debe haber los ficheros Python que considere oportunos para la resolución de la práctica.

Además, el proyecto **deberá incluir un README** explicando el funcionamiento de la práctica y respondiendo a las siguientes preguntas:
1. Explique qué es la Programación Orientada a Objetos (POO), qué es una clase, y qué es una instancia. Después, explique cómo se podría aplicar POO a esta práctica.
2. Explique la diferencia entre una función y un método.
3. Explique too lo que sepa acerca de los constructores (qué son, para qué sirven) en POO. Razone si un constructor en Python puede no tener parámetros.
4. Explique las diferencias entre una lista y un diccionario en Python. Indique cómo se utilizarían estas estructuras de datos para almacenar los datos de las reservas en esta práctica.


## Criterios de corrección
Para la correción de la práctica, se tendrán en cuenta los siguientes aspectos:
-  Documentación incluida en el README del repositorio (respondiendo a las preguntas planteadas).
-  Claridad y calidad del código.
-  Utilización de los conceptos teóricos vistos hasta la fecha (entre ellos los relativos a estructuras de datos).
-  *Extra*: Uso de la librería Pandas para la gestión de los datos de las reservas.
-  *Extra*: Uso de Programación Orientada a Objetos para modelar los datos.


# Introducción

## Ejercicio 1

Empezarás leyendo la información de los ficheros booked_business.csv y booked_regular.csv (que encontrarás en el aula virtual y en este repositorio) que contiene únicamente la información de los asientos de business y turista que están reservados. Los ficheros de reserva CSV tienen el siguiente formato.

DNI, Nombre, Apellidos, Asiento, Edad, Facturacion

Estos campos son del siguiente tipo:

  - DNI: Alfanumérico, máximo 9 caracteres.

  - Nombre: String, máximo 20 caracteres.

  - Apellidos: String, máximo 30 caracteres.

  - Asiento: Alfanumérico, 1 o 2 caracteres número, y 1 para letra.

  - Edad: Numérico [0-120]

  - Facturación (de maleta): Si / No

El avión dispone de 20 filas, y cada fila 6 asientos (A,B,C,D,E,F). A y F corresponden a ventanilla, C y D a pasillo. Las primeras 8 filas serán siempre de business y el resto de turista. Lo primero que debe hacer tu aplicación es mostrar la información de los asientos (R: reservado, L: libre) de la siguiente manera:

````{verbatim}
       A B C      D E F
==========================
1      R L R      L L L
2      L R L      L L L
…
8      L L L      L L L
--------------------------
…
20     R L L      R L R
````

Además debes mostrar el siguiente resumen:

````{verbatim}
El vuelo está un XX% ocupado.
YY pasajeros alojados en business y ZZ pasajeros alojados en turista.
NN pasajeros facturan maleta.
````

Por último, si encuentras algún conflicto en los datos leídos:

- Una línea leída con campos incompletos
- Un mismo asiento asignado a 2 o más pasajeros

Por ejemplo:

````{verbatim}

ATENCIÓN: El asiento XX está asignado a N pasajeros:
    - Nombre Apellidos Pasajero 1
    - Nombre Apellidos Pasajero 2
    …
    - Nombre Apellidos Pasajero N
````
    
## Menú

Genera el siguiente menú interactivo para tu aplicación:

````{verbatim}
1. Mostrar Información Reservas
2. Mostrar Pasajeros
3. Añadir Reserva
4. Modificar Reserva
5. Eliminar Reserva
   Introduce una opción:
````

   
El punto 2 debe preguntar si se desea mostrar la información ordenada por número de asiento o por orden alfabético (apellido) de los asientos ocupados. En cualquier caso, la salida esperada es la siguiente (campos separados por tabulador):

    Asiento      Apellidos      Nombre

## Sistema de Reservas

Debes permitir añadir, modificar o eliminar reservas de un vuelo. Implementa un menú de texto donde permita realizar este tipo de operaciones.

  - Añadir: Debe preguntar por todos los datos y guardarlos correctamente.

    - Se debe preguntar si quiere reservar en business o turista.

    - Se debe preguntar si quiere Ventanilla/Pasillo/Indiferente.

    - Con estos datos, la asignación del asiento se ha de hacer de manera automática, asignando el primer asiento libre con las características anteriormente descritas.

  - Modificar: Debe preguntar por el DNI, comprobar que ya existe esa reserva, y deberá permitir modificar únicamente el asiento asignado. La asignación del asiento se realizará directamente especificando el asiento. Si éste está libre, se re-asignará. Si no hay reserva con ese DNI, debe notificarlo.

  - Eliminar: Debe preguntar por el DNI, comprobar que existe la reserva y eliminarla.

Con cada operación que cambie el estado de las reservas se debe realizar 2 acciones:

  - Mostrar la información únicamente de la fila del avión donde se ha añadido o eliminado la reserva. En el caso de modificado, se mostrarán las 2 filas (donde estaba la reserva, y donde se ha reasignado). Si la fila fuera la misma, solo se mostrará una vez.

  - Además se debe persistir/guardar la información en los ficheros  booked_business.csv y booked_regular.csv. Sólo se debe guardar los asientos que están reservados.


